---
title: Contribution aux projets de recherche scientifique à Strasbourg
---

## De nombreux·ses strasbourgeois·e·s sont prêt·e·s à contribuer aux projets de recherche !

Identifions là où des coups de mains de personnes motivées permettraient des avancées dans les projets scientifiques.

Et décrivons des **[missions de contribution](missions/)** pour permettre aux personnes motivées de contribuer concrètement.

Organisons régulièrement des sessions où différentes missions de contribution sont proposées.

Ces moments de synchronisation permettent de mettre en lumière les contributions qui ont été réalisées et permettent la transmission des savoir-faire en pair-à-pair par 
les personnes qui ont réalisé des missions.

La page dédiée à la contribution et le chat mis en place permettent la poursuite de la contribution de manière asynchrone, lorsque chacun·e en a le temps,
tout en maintenant la dynamique active.

## Forums de contribution 2020

Première date : https://facebook.com/events/233263841042087/

Organisations présentes : https://gitlab.com/contribution.coop/contribution.coop.gitlab.io/-/milestones/1

Un forum de contribution rassemble des porteurs de projets et des personnes qui souhaitent apporter une contribution concrète
au projet de leur choix, en étant guidé·e·s dans la réalisation de la mission par les porteurs de projet.

Plusieurs forums de contribution auront lieu en mars et avril pour préfigurer un nombre limité de missions de contribution.

Des forums avec un grand nombre de missions de contribution proposées auront lieu par la suite.

Soumettez sans attendre une mission de contribution ou simplement un point de friction dans vos projets de recherche où des contributions seraient bienvenues :

- **[Soumettre une mission](https://gitlab.com/contribution.recherche-scientifique-strasbourg/contribution.recherche-scientifique-strasbourg.gitlab.io/issues/new?issuable_template=mission)**

- **[Soumettre une friction](https://gitlab.com/contribution.recherche-scientifique-strasbourg/contribution.recherche-scientifique-strasbourg.gitlab.io/issues/new?issuable_template=friction)**

Pour toute demande d'information, écrivez à l'adresse [jibe.boh@orange.fr](mailto:jibe.boh@orange.fr)

## Accompagnement à la définition de missions de contribution

Une mission de contribution part d'une friction (une étape dans votre process qui n'est pas idéale parce que l'équipe-cœur seule ne peut pas l'assurer facilement).

Pour y répondre, l'action à proposer doit rester :
- limitée dans le temps, l'énergie à y dédier et l'importance pour le projet (pour permettre un passage à l'action facile et ne pas prendre de risque important)
- adaptée à l'ancrage de la personne qui contribue (préférez de loin inviter une personne à contribuer au volet qui correspond à sa ville qu'à une ville à l'autre bout du monde) - sauf quand ça a des impacts positifs, comme faire découvrir
- coordonnée au travers de tableaux de bord et de discussions sur un chat et en direct)

À vous de choisir sous quelle forme, mais la mission doit également apporter à un·e contributeur·ice. À vous de jauger entre l'apprentissage, la découverte d'un domaine et l'établissement de liens avec les acteurs…

La réalisation de la mission par une ou plusieurs personnes lors du forum offre également une opportunité d'entraide entre les contributeur·ices.

Ça vous parle ? Vous avez la méthode, à vous de définir et décrire une mission !

Les missions proposées lors des forums de contribution doivent être réellement adaptées, réalisables et proposer un cadre de réciprocité qui satisfait les contributeur·ices.

Un service vous est proposé pour affiner une mission que vous aurez commencée à définir et décrire. Il s'agit de tester la mission auprès de participant·e·s de test (les autres personnes qui définissent des missions), en tirer des retours quantitatifs sur chaque étape de la mission proposée et améliorer les missions en conséquence.

Contactez [jibe.boh@orange.fr](mailto:jibe.boh@orange.fr) pour en savoir plus sur ce service. Atelier d'amélioration de missions avec ces méthodes d'expérience utilisateur·ice à venir à Strasbourg au cours du mois de mars.

## Financement

Que vous soyez porteur de projet, un·e citoyen·ne ou une organisation : si vous souhaitez qu'il y ait une dynamique de contribution aux projets de l'écosystème strasbourgeois et à des Biens Communs : **participez financièrement à la cagnotte mutualisée**
En participant financièrement et en proposat une mission de contribution, vous accédez à la contrepartie « notoriété » : le nom de votre organisation, votre logo, une punchline et votre mission de contribution phare seront présents sur les supports de communication.

Ces supports de communication seront diffusés sur les réseaux sociaux et sous forme physique. Cela donnera de la visibilité à votre organisation.

Vous accédez également à la contrepartie « contribution réalisée » (sans garantie) : des sympathisant·e·s de votre projet contribueront concrètement

La somme mutualisée servira à rétribuer différentes formes de contribution :
- les contributions à l'organisation des forums,
- les contributions qui dépassent un cadre bénévole

Ces rétributions sont soumises à modération a priori par le collège des financeurs.